﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DapperRepository.Repositories
{
    public interface IRepository
    {
        long Add<T>(T entity) where T : class;

        Task<int> AddAsync<T>(T entity) where T : class;

        bool Update<T>(T entity) where T : class;

        Task<bool> UpdateAsync<T>(T entity) where T : class;

        Task<int> ExecuteAsync(string sql, object parms = null);

        int Execute(string sql, object parms = null, CommandType commandType = CommandType.Text);

        T Get<T>(int key) where T : class;

        bool Delete<T>(T entity) where T : class;

        //int Execute(string sql, object parms = null);
    }
}
