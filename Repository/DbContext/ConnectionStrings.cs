﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DapperRepository.DbContext
{
    public class ConnectionStrings
    {
        public string TestConnectionString { set; get; }
        public string WriteConnectionString { set; get; }
        public string ReadConnectionString { set; get; }
    }
}
