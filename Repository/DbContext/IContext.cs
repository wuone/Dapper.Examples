﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DapperRepository.DbContext
{
    public interface IContext : IDisposable
    {
        //IDbConnection Conn { get; }
        IDbConnection GetDbConnection();
    }
}
