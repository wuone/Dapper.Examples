﻿using System;
using System.Collections.Generic;
using System.Text;
using Test.Models;
using Tests.Models;
using Xunit;

namespace Tests.Repositories
{
    public class RepositoryTest: BaseTest
    {
        /// <summary>
        /// 单个添加
        /// </summary>
        [Fact]
        public void AddTest()
        {
            var user = new User { UserName= Guid.NewGuid().ToString(),Age=25,ModifyTime=DateTime.Now};
            long res = 0;
            using (var context = Context)
            {
                res = new DapperRepository.Repositories.Repository(Context).Add(user);
            } 
            Assert.True(res>0);
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        [Fact]
        public void AddMoreTest()
        {
            var users = new List<User>();
            for (var i = 0; i < 5; i++)
            {
                users.Add(new User { UserName = Guid.NewGuid().ToString(), Age = i });
            }
            long res = 0;
            using (var context = Context)
            {
                res = new DapperRepository.Repositories.Repository(Context).Add(users);
            }
            Assert.True(res > 0);
        }


        /// <summary>
        /// 批量更新，使用Where In
        /// </summary>
        [Fact]
        public void UpdateUsersByWhereInTest()
        {
            var sql = @"UPDATE dbo.[user] SET isvalid=0 WHERE UserId IN @UserId ;";
            var userIdArr = new int[] { 3013, 3012, 3011, 3010 };
            var res = Repository.Execute(sql, new { UserId = userIdArr });
            Assert.True(res > 0);
        }
        /// <summary>
        /// 批量更新，分别把用户Id是3013的用户名称更新为张三，用户Id是3012的用户名称更新为李四
        /// </summary>
        [Fact]
        public void UpdateUsersTest()
        {
            var sql = @"UPDATE dbo.[user] SET UserName=@UserName WHERE UserId = @UserId ;";
            var users = new List<User> {
                new User { UserId=3013, UserName = "张三", },
                new User { UserId=1, UserName = "张三1", },
                new User { UserId =3012, UserName = "李四",  },
           };
            var res = Repository.Execute(sql,users);
            Assert.True(res > 0);
        }
        [Fact]
        public void UpdateUserTest()
        {
            var user = new User { UserName = Guid.NewGuid().ToString(), Age = 25 ,UserId=1,ModifyTime=DateTime.Now};
            bool res = false;
            user.ModifyTime = user.ModifyTime.AddDays(1);
            using (var context = Context)
            {
                res = new DapperRepository.Repositories.Repository(Context).Update(user);
            }
            Assert.True(res);
        }
        /// <summary>
        /// 批量循环更新
        /// </summary>
        [Fact]
        public void UpdateUsers1Test()
        {
            var sql = @"UPDATE dbo.[user] SET UserName=@UserName WHERE UserId = @UserId ;";
            var users = new List<User> {
                new User { UserId=3013, UserName = "张三", },
                new User { UserId =3012, UserName = "李四",  },
                new User { UserId=1, UserName = "张三1", },
           };
            var res = false;
            for (int i = 0; i < users.Count; i++)
            {
                var temp = Repository.Execute(sql, users[i])> 0;
                res &= temp;
            }
            
            Assert.True(res);
        }

        /// <summary>
        /// 获取一条数据
        /// </summary>
        [Fact]
        public void GetTest()
        {
            var user = Repository.Get<User>(1);
            Assert.NotNull(user);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        [Fact]
        public void DeleteTest()
        {
            var res = Repository.Delete(new User { UserId = 4010 });
            Assert.True(res);
        }

        /// <summary>
        /// 删除多条数据
        /// </summary>
        [Fact]
        public void DeleteMoreTest()
        {
            var users = new List<User> {
                new User { UserId=4010, UserName = "张三", },
                new User { UserId=4011, UserName = "张三1", },
                new User { UserId =4012, UserName = "李四",  },
           };
            var res = Repository.Delete(users);
            Assert.True(res);
        }


        

    }
}
