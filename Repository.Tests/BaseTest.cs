﻿

using DapperRepository.DbContext;
using DapperRepository.Repositories;

namespace Tests
{
    public class BaseTest
    {
        private readonly string constr = "Password=123456;Persist Security Info=True;User ID=sa;Initial Catalog=Test;Data Source=localhost";
        private ConnectionStrings ConnectionString =>new ConnectionStrings { TestConnectionString= constr };
        protected IContext Context => new RepositoryContext(ConnectionString);
        protected IRepository Repository => new Repository(Context);

        protected Repository Rep => new Repository(Context);
    }
}
