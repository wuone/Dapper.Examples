﻿using System;
using System.Collections.Generic;
using System.Text;
using Tests.DapperTests.Models;
using Xunit;

namespace Tests.DapperTests
{
    public class ExecuteTest:BaseTest
    {
        /// <summary>
        /// 执行单次存储过程
        /// </summary>
        [Fact]
        public void ExecuteSingleSpInsertTest()
        {
            var sp = "Invoice_Insert";
            var singleParam = new { Kind = InvoiceKind.WebInvoice, Code = "Single_Sp_Insert_1" };
            var affectedRows = Rep.Execute(sp, singleParam, System.Data.CommandType.StoredProcedure);
            Assert.True(affectedRows > 0);
        }
        /// <summary>
         /// 执行多次存储过程
         /// </summary>
        [Fact]
        public void ExecuteManySpInsertTest()
        {
            var sp = "Invoice_Insert";
            var param = new[]
            {
               new { Kind = InvoiceKind.WebInvoice, Code = "Many_Sp_Insert_1" },
               new { Kind = InvoiceKind.StoreInvoice, Code = "Many_Sp_Insert_2" },
               new { Kind = InvoiceKind.StoreInvoice, Code = "Many_Sp_Insert_3" },
             };
            var affectedRows = Rep.Execute(sp, param, System.Data.CommandType.StoredProcedure);
            Assert.True(affectedRows > 0);
        }
        /// <summary>
        /// 插入一条，通过 Insert 语句
        /// </summary>
        [Fact]
        public void ExecuteSingleSqlInsertTest()
        {
            string sql = "INSERT INTO Invoice (Code) Values (@Code);";
            var singleParam = new { Kind = InvoiceKind.WebInvoice, Code = "Single_Sql_Insert_1" };
            var affectedRows = Rep.Execute(sql, singleParam);
            Assert.True(affectedRows > 0);
        }
        /// <summary>
        /// 插入多条，通过 Insert 语句
        /// </summary>
        /// </summary>
        [Fact]
        public void ExecuteManySqlInsertTest()
        {
            string sql = "INSERT INTO Invoice (Code) Values (@Code);";
            var param = new[]
            {
               new { Kind = InvoiceKind.WebInvoice, Code = "Many_Sql_Insert_1" },
               new { Kind = InvoiceKind.StoreInvoice, Code = "Many_Sql_Insert_2" },
               new { Kind = InvoiceKind.StoreInvoice, Code = "Many_Sql_Insert_3" },
            };
            var affectedRows = Rep.Execute(sql, param);
            Assert.True(affectedRows > 0);
        }

        /// <summary>
        /// 更新一条，通过 Update 语句
        /// </summary>
        [Fact]
        public void ExecuteSingleSqlUpdateTest()
        {
            string sql = "UPDATE Invoice SET Code = @Code WHERE InvoiceID = @InvoiceID;";
            var singleParam = new { InvoiceID = 1, Code = "Single_Sql_Update_1" };
            var affectedRows = Rep.Execute(sql, singleParam);
            Assert.True(affectedRows > 0);
        }
        /// <summary>
        /// 更新多条，通过 Update 语句
        /// </summary>
        /// </summary>
        [Fact]
        public void ExecuteManySqlUpdateTest()
        {
            string sql = "UPDATE Invoice SET Code = @Code WHERE InvoiceID = @InvoiceID;";
            var param = new[]
            {
               new { InvoiceID =1, Code = "Many_Sql_Update_1" },
               new { InvoiceID =2, Code = "Many_Sql_Update_2" },
               new { InvoiceID =3, Code = "Many_Sql_Update_3" },
            };
            var affectedRows = Rep.Execute(sql, param);
            Assert.True(affectedRows > 0);
        }

        /// <summary>
        /// 删除一条，通过 Delete 语句
        /// </summary>
        [Fact]
        public void ExecuteSingleSqlDeleteTest()
        {
            string sql = "DELETE FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var singleParam = new { InvoiceID = 1 };
            var affectedRows = Rep.Execute(sql, singleParam);
            Assert.True(affectedRows > 0);
        }
        /// <summary>
        /// 更新多条，通过 Delete 语句
        /// </summary>
        /// </summary>
        [Fact]
        public void ExecuteManySqlDeleteTest()
        {
            string sql = "DELETE FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var param = new[]
            {
               new { InvoiceID =1 },
               new { InvoiceID =2 },
               new { InvoiceID =3 },
            };
            var affectedRows = Rep.Execute(sql, param);
            Assert.True(affectedRows > 0);
        }
    }
}
