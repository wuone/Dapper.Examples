﻿using System;
using System.Collections.Generic;
using System.Text;
using Tests.DapperTests.Models;
using Xunit;

namespace Tests.DapperTests
{
    public class Query2Test:BaseTest
    {
        [Fact]
        public void QueryFirstMapToAnonymousTest()
        {
            string sql = "SELECT * FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var res = Rep.QueryFirst(sql, new { InvoiceID = 4 });
            Assert.NotNull(res);
                 
        }

        [Fact]
        public void QueryFirstMapToStronglyTypedTest()
        {
            string sql = "SELECT * FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var res = Rep.QueryFirst<Invoice>(sql, new { InvoiceID = 4 });
            Assert.NotNull(res);
        }


        [Fact]
        public void QueryFirstOrDefaultTest()
        {
            string sql = "SELECT * FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var res = Rep.QueryFirstOrDefault<Invoice>(sql, new { InvoiceID = 4 });
            Assert.NotNull(res);
        }


        [Fact]
        public void QuerySingleTest()
        {
            string sql = "SELECT * FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var res = Rep.QuerySingle<Invoice>(sql, new { InvoiceID = 4 });
            Assert.NotNull(res);
        }

        [Fact]
        public void QuerySingleOrDefaultTest()
        {
            string sql = "SELECT * FROM Invoice WHERE InvoiceID = @InvoiceID;";
            var res = Rep.QuerySingleOrDefault<Invoice>(sql, new { InvoiceID = 4 });
            Assert.NotNull(res);
        }

    }
}
