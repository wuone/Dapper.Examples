﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.DapperTests.Models
{
    public class Invoice
    {
        public int InvoiceID { get; set; }
        public string Code { get; set; }
        public InvoiceKind Kind { get; set; }
        public List<InvoiceItem> Items { get; set; }
        public InvoiceDetail Detail { get; set; }
    }
}
