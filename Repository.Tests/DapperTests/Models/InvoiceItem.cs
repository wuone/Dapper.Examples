﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.DapperTests.Models
{
    public class InvoiceItem
    {
        public int InvoiceItemID { get; set; }
        public int InvoiceID { get; set; }

        public string Code { get; set; }
    }
}
