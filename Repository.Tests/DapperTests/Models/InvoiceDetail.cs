﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.DapperTests.Models
{
    public class InvoiceDetail
    {
        public int InvoiceID { get; set; }
        public string Detail { get; set; }
    }
}
