﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.DapperTests.Models
{
    public enum InvoiceKind
    {
        StoreInvoice = 1,
        WebInvoice = 2
    }
}
