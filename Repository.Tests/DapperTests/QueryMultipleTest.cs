﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tests.DapperTests.Models;
using Xunit;

namespace Tests.DapperTests
{
    public class QueryMultipleTest:BaseTest
    {
        /// <summary>
        /// 执行多条查询语句，返回多个结果
        /// </summary>
        [Fact]
        public void GetMultipleReslutTest()
        {
            string sql = "SELECT * FROM Invoice WHERE InvoiceID = @InvoiceID; SELECT * FROM InvoiceItem WHERE InvoiceID = @InvoiceID;";

            var multi=Rep.QueryMultiple(sql, new { InvoiceID = 4 });
            var invoice = multi.Read<Invoice>().First();
            var invoiceItems = multi.Read<InvoiceItem>().ToList();
            Assert.True(invoice!=null && invoiceItems.Any());
        }
    }
}
