﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tests.DapperTests.Models;
using Xunit;

namespace Tests.DapperTests
{
    public class QueryTest:BaseTest
    {
        [Fact]
        public void GetAnonymousTest()
        {
            var sql = @"SELECT * FROM Invoice;";
            var res = Rep.Query(sql);
            Assert.NotNull(sql);
        }

        [Fact]
        public void GetStronglyTypedTest()
        {
            var sql = @"SELECT * FROM Invoice;";
            var res = Rep.Query<Invoice>(sql);
            Assert.NotNull(sql);
        }

        [Fact]
        public void GetMultiMappingOneToOneTest()
        {
            var sql = "SELECT * FROM Invoice AS A INNER JOIN InvoiceDetail AS B ON A.InvoiceID = B.InvoiceID;";
            var invoices = Rep.Query<Invoice, InvoiceDetail, Invoice>(
            sql,
            (invoice, invoiceDetail) =>
            {
                invoice.Detail = invoiceDetail;
                return invoice;
            },
            splitOn: "InvoiceID");
            Assert.NotNull(sql);
        }

        [Fact]
        public void GetMultiMappingOneToManyTest()
        {
            string sql = "SELECT * FROM Invoice AS A INNER JOIN InvoiceItem AS B ON A.InvoiceID = B.InvoiceID;";
            var invoiceDictionary = new Dictionary<int, Invoice>();
            var res=Rep.Query<Invoice, InvoiceItem, Invoice>(
                    sql,
                    (invoice, invoiceItem) =>
                    {
                        Invoice invoiceEntry;
                        if (!invoiceDictionary.TryGetValue(invoice.InvoiceID, out invoiceEntry))
                        {
                            invoiceEntry = invoice;
                            invoiceEntry.Items = new List<InvoiceItem>();
                            invoiceDictionary.Add(invoiceEntry.InvoiceID, invoiceEntry);
                        }

                        invoiceEntry.Items.Add(invoiceItem);
                        return invoiceEntry;
                    },
                    splitOn: "InvoiceItemID")
                    .Distinct();
            Assert.True(res!=null);
        }
    }
}
