﻿using System;
using System.Collections.Generic;
using System.Text;
using Tests.Models;
using Xunit;

namespace Tests
{
    public class CarTest : BaseTest
    {
        /// <summary>
        /// 添加单条Car数据
        /// </summary>
        [Fact]
        public void AddCarTest()
        {
            var car = new Car { Id = 0, Name = "Volvo" };
            var res = Repository.Add(car) > 0;
            Assert.True(res);
        }
        /// <summary>
        /// 添加多条Car数据
        /// </summary>
        [Fact]
        public void AddCarsTest()
        {
            List<Car> cars = new List<Car> {
                new Car { Id = 0, Name = "Volvo" },
                new Car { Id = 0, Name = "Volvo1" }
            };
            var res = Repository.Add(cars) > 0;
            Assert.True(res);
        }
        /// <summary>
        /// 更新单条Car数据
        /// </summary>
        [Fact]
        public void UpdateCarTest()
        {
            var insertCar = new Car {  Name = "Volvo" };
            var id = Repository.Add(insertCar);
            var car = new Car {  Id= Convert.ToInt32(id),  Name = "car Update" };
            var res = id>0 && Repository.Update(car);
            Assert.True(res);
        }
        /// <summary>
        /// 获取单条Car数据
        /// </summary>
        [Fact]
        public void GetCarTest()
        {
            var insertCar = new Car { Name = "Volvo" };
            var id = Repository.Add(insertCar);
            var res = Repository.Get<Car>(Convert.ToInt32(id));
            Assert.NotNull(res);
        }

        /// <summary>
        /// 删除单条Car数据
        /// </summary>
        [Fact]
        public void DeleteCarTest()
        {
            var insertCar = new Car { Name = "Volvo" };
            var id = Repository.Add(insertCar);
            var res = Repository.Delete(new Car { Id = Convert.ToInt32(id) });
            Assert.True(res);
        }
    }
}
