﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.Models
{
    [Table("dbo.[User]")]
    public class User
    {        
        public string UserName { set; get; }

        [Key]
        public int UserId { set; get; }
        [Write(false)]
        public int Age { set; get; }
  
        [Write(false)]
        public DateTime ModifyTime { set; get; }
        //public int IsValid { set; get; }
    }
}
