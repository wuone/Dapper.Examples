﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Models
{
    public class Car
    {        
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
